//Command to deploy the slidewikicloud respository in the cloud
appcfg.py update slidewiki/

//To deploy the db schema in the cloud sql, we need to create a bucket which contain the sql dump file 

//Command to creation of bucket
gsutil.py mb gs://slidewiki

//Command to upload the sql dump to the bucket
gsutil.py cp slidewiki.sql gs://slidewiki

//Command to check the sql file in the cloud
gsutil.py ls -l gs://slidewiki

//Create a DB Cloud Instance in google admin console and choose the profile server
//Command to import the sql dump file in cloud db
gs://slidewiki/slidewiki.sql



Examples:
http://slidewikicloud.appspot.com/documentation/
http://slidewikicloud.appspot.com/deck/750_semantic
http://slidewikicloud2.appspot.com/documentation/
http://slidewikicloud.appspot.com/ajax/getDeckTree/deck/56?id=0



ADMIN GOOGLE APP CLOUD INTERFACE

//Setup of the cloud application
https://appengine.google.com/settings?&app_id=s~slidewikicloud&version_id=1.375578458759159763

//Management the DB Cloud Instances 
https://console.developers.google.com/project/apps~slidewikicloud/sql

//Cost using the cloud
https://console.developers.google.com/billing/814947179201/history
